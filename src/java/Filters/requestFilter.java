package Filters;
import java.io.*;
import javax.servlet.*;
public class requestFilter implements Filter{
 public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)throws IOException, ServletException{
 
      response.getWriter().println("Before filter");
      chain.doFilter(request,response);
      response.getWriter().println("After filter");
 }   
 public void init(FilterConfig filterConfig) throws ServletException {
     
 }
 public void destroy() {
     
 }
}